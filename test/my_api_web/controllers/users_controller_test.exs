defmodule MyApiWeb.UsersControllerTest do
  use MyApiWeb.ConnCase

  test "deve retornar 200", %{conn: conn} do
    conn = get(conn, ~p"/api")
    assert json_response(conn, 200) == %{"id" => 1, "name" => "windson"}
  end

  test "deve retornar o nome correto", %{conn: conn} do
    conn = get(conn, ~p"/api?name=jedson")
    assert json_response(conn, 200) == %{"id" => 1, "name" => "jedson"}
  end

  test "deve retornar 404", %{conn: conn} do
    conn = get(conn, ~p"/api/teste")
    assert json_response(conn, 404)
  end

end
