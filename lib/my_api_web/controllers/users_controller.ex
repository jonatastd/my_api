defmodule MyApiWeb.UsersController do
  use MyApiWeb, :controller

  def index(conn, params) do
    render(conn, name: Map.get(params, "name", "windson"))
  end
end